import logoLineAdWhite from './svg/logo-line-ad-white.svg';
import logoLineAdDark from './svg/logo-line-ad-dark.svg';

const Icons = {
    logoLineAdWhite: logoLineAdWhite,
    logoLineAdDark: logoLineAdDark,
};

export default Icons;
