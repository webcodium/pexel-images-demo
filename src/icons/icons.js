import iconTabTemplate from './svg/icon-tab-template.svg';
import iconTabText from './svg/icon-tab-text.svg';
import iconTabShapes from './svg/icon-tab-shapes.svg';
import iconTabBackground from './svg/icon-tab-background.svg';
import iconTabImages from './svg/icon-tab-images.svg';
import iconTabButton from './svg/icon-tab-button.svg';
import iconTabUpload from './svg/icon-tab-upload.svg';
import iconTabVideo from './svg/icon-tab-video.svg';

import iconTabLines from './svg/icon-tab-lines.svg';
import iconTabIcons from './svg/icon-tab-icons.svg';

import iconToolColor from './svg/icon-tool-color.svg';
import iconToolDelete from './svg/icon-tool-delete.svg';
import iconToolLayer from './svg/icon-tool-layer.svg';
import iconToolLink from './svg/icon-tool-link.svg';

import iconAlignLeft from './svg/icon-align-left.svg';
import iconAlignCenter from './svg/icon-align-center.svg';
import iconAlignRight from './svg/icon-align-right.svg';

import iconToolLayerBack from './svg/icon-tool-layer-back.svg';
import iconToolLayerForward from './svg/icon-tool-layer-forward.svg';

import iconDropZonePic from './svg/icon-dropzone-pic.svg';

import iconEye from './svg/icon-eye.svg';
import iconUndoArrow from './svg/icon-undo-arrow.svg';
import iconPencil from './svg/icon-pencil.svg';
import iconDndVertical from './svg/icon-dnd-vertical-align.svg';

import iconReplay from './svg/icon-replay.svg';
import iconAdd from './svg/icon-add.svg';
import iconDrop from './svg/icon-drop.svg';

import iconSearch from './svg/icon-search.svg';
import iconDownload from './svg/icon-download.svg';
import iconEmbed from './svg/icon-embed.svg';
import iconDuplicate from './svg/icon-duplicate.svg';
import iconUndo from './svg/icon-undo.svg';
import iconInvertColors from './svg/icon-invert-colors.svg';

import iconCheckbox from './svg/icon-checkbox.svg';
import iconCheckboxChecked from './svg/icon-checkbox-checked.svg';
import iconClose from './svg/icon-close.svg';

import iconFBLike from './svg/icon-fb-like.svg';
import iconFBComment from './svg/icon-fb-comment.svg';
import iconFBShare from './svg/icon-fb-share.svg';

import iconTemplate from './svg/icon-template.svg';

const Icons = {
    iconTabTemplate: iconTabTemplate,
    iconTabText: iconTabText,
    iconTabShapes: iconTabShapes,
    iconTabBackground: iconTabBackground,
    iconTabImages: iconTabImages,
    iconTabButton: iconTabButton,
    iconTabVideo: iconTabVideo,
    iconTabUpload: iconTabUpload,
    iconTabLines: iconTabLines,
    iconTabIcons: iconTabIcons,
    iconToolColor: iconToolColor,
    iconToolDelete: iconToolDelete,
    iconToolLayer: iconToolLayer,
    iconToolLink: iconToolLink,
    iconAlignLeft: iconAlignLeft,
    iconAlignCenter: iconAlignCenter,
    iconAlignRight: iconAlignRight,
    iconToolLayerBack: iconToolLayerBack,
    iconToolLayerForward: iconToolLayerForward,
    iconDropZonePic: iconDropZonePic,
    iconEye: iconEye,
    iconUndoArrow: iconUndoArrow,
    iconPencil: iconPencil,
    iconDndVertical: iconDndVertical,
    iconReplay: iconReplay,
    iconAdd: iconAdd,
    iconDrop: iconDrop,
    iconSearch: iconSearch,
    iconDownload: iconDownload,
    iconCheckbox: iconCheckbox,
    iconCheckboxChecked: iconCheckboxChecked,
    iconEmbed: iconEmbed,
    iconDuplicate: iconDuplicate,
    iconUndo: iconUndo,
    iconInvertColors: iconInvertColors,
    iconClose: iconClose,
    iconFBLike: iconFBLike,
    iconFBComment: iconFBComment,
    iconFBShare: iconFBShare,
    iconTemplate: iconTemplate
};

export default Icons;
