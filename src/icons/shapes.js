// shapes
import shapeSquareFilled from './svg/shapes/shape-square-filled.svg';
import shapeSquareBorderTiny from './svg/shapes/shape-square-border-tiny.svg';
import shapeSquareBorderMiddle from './svg/shapes/shape-square-border-middle.svg';
import shapeSquareBorderHuge from './svg/shapes/shape-square-border-huge.svg';
import shapeSquareFilledRound from './svg/shapes/shape-square-filled-round.svg';
import shapeSquareBorderTinyRound from './svg/shapes/shape-square-border-tiny-round.svg';
import shapeSquareBorderMiddleRound from './svg/shapes/shape-square-border-middle-round.svg';
import shapeSquareBorderHugeRound from './svg/shapes/shape-square-border-huge-round.svg';

import shapeCircleFilled from './svg/shapes/shape-circle-filled.svg';
import shapeCircleBorder from './svg/shapes/shape-circle-border.svg';
import shapeLineTiny from './svg/shapes/shape-line-tiny.svg';
import shapeLineHuge from './svg/shapes/shape-line-huge.svg';

// svg
import shapeTriangleFilled from './svg/shapes/shape-triangle-filled.svg';
import shapeTriangleTiny from './svg/shapes/shape-triangle-tiny.svg';
import shapeTriangleMiddle from './svg/shapes/shape-triangle-middle.svg';
import shapeTriangleHuge from './svg/shapes/shape-triangle-huge.svg';

// patterns
import patternDiamondRed from './svg/patterns/pattern-diamond-red.png';
import patternCircleGreen from './svg/patterns/pattern-circle-green.png';
import patternTriangleBlue from './svg/patterns/pattern-triangle-blue.png';

const Shapes = {
    shapeSquareFilled: shapeSquareFilled,
    shapeSquareBorderTiny: shapeSquareBorderTiny,
    shapeSquareBorderMiddle: shapeSquareBorderMiddle,
    shapeSquareBorderHuge: shapeSquareBorderHuge,
    shapeSquareFilledRound: shapeSquareFilledRound,
    shapeSquareBorderTinyRound: shapeSquareBorderTinyRound,
    shapeSquareBorderMiddleRound: shapeSquareBorderMiddleRound,
    shapeSquareBorderHugeRound: shapeSquareBorderHugeRound,
    shapeCircleFilled: shapeCircleFilled,
    shapeCircleBorder: shapeCircleBorder,
    shapeLineTiny: shapeLineTiny,
    shapeLineHuge: shapeLineHuge,
    shapeTriangleFilled: shapeTriangleFilled,
    shapeTriangleTiny: shapeTriangleTiny,
    shapeTriangleMiddle: shapeTriangleMiddle,
    shapeTriangleHuge: shapeTriangleHuge,
    patternDiamondRed:patternDiamondRed,
    patternCircleGreen:patternCircleGreen,
    patternTriangleBlue:patternTriangleBlue
};

export default Shapes;