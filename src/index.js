import React from 'react';
import {render} from 'react-dom';

import {Provider} from 'react-redux';
import {createStore} from 'redux';
import rootReducer from './reducers';

import injectTapEventPlugin from 'react-tap-event-plugin';
import ConfigService from './services/configService';


import 'url-polyfill/url-polyfill.min.js';

import './../node_modules/reset-css/reset.css';
import './../node_modules/normalize.css/normalize.css';

import App from './App';

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();


ConfigService.loadConfig().then((data) => {
    const initialState = {
        config: data
    };
    const store = createStore(rootReducer, initialState);
    render(
        <Provider store={store}>
            <App/>
        </Provider>,
        document.getElementById('root')
    )
});