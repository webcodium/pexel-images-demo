import {STORE_CONFIG, UPDATE_CONFIG_VAL} from "./actionTypes";

export const storeConfig = (config) => {
    return {
        type: STORE_CONFIG,
        config
    }
};

export const updateConfigValue = (next) => {
    return {
        type: UPDATE_CONFIG_VAL,
        next
    }
};

