// config
export const STORE_CONFIG = 'STORE_CONFIG';
export const UPDATE_CONFIG_VAL = 'UPDATE_CONFIG_VAL';

// sidebar
export const STORE_SIDEBAR = 'STORE_SIDEBAR';
export const SET_SIDEBAR_CURRENT = 'SET_SIDEBAR_CURRENT';
// images sidebar
export const STORE_IMAGES = 'STORE_IMAGES';
export const ADD_NEW_IMAGES = 'ADD_NEW_IMAGES';
export const DELETE_IMAGE = 'DELETE_IMAGE';

// dropzone loader
export const SET_DROPZONE_LOADER = 'SET_DROPZONE_LOADER';