import {SET_DROPZONE_LOADER} from "./actionTypes";

export const setDropzoneLoader = (loader) => {
    return {
        type: SET_DROPZONE_LOADER,
        loader
    }
};