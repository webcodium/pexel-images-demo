import {
    STORE_SIDEBAR,
    SET_SIDEBAR_CURRENT,
    STORE_IMAGES,
    ADD_NEW_IMAGES,
    DELETE_IMAGE
} from "./actionTypes";

export const storeSidebar = (sidebar) => {
    return {
        type: STORE_SIDEBAR,
        sidebar
    }
};

export const setSidebarCurrent = (current) => {
    return {
        type: SET_SIDEBAR_CURRENT,
        current
    }
};

// store sidebar images

export const storeImages = (images) => {
    return {
        type: STORE_IMAGES,
        images
    }
};

// add new image to sidebar

export const addNewImages = (images) => {
    return {
        type: ADD_NEW_IMAGES,
        images
    }
};

// delete image from sidebar

export const deleteImage = (image) => {
    return {
        type: DELETE_IMAGE,
        image
    }
};







