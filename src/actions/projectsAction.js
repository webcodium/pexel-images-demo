export const storeProjects = (projects) => {
    return {
        type: 'STORE_PROJECTS',
        projects
    }
};

export const updateProject = (project) => {
    return {
        type: 'UPDATE_PROJECT',
        project
    }
};

export const clearProjects = () => {
    return {
        type: 'CLEAR_PROJECTS',
        projects: []
    }
};