export const delayExecution = ms => {
    let id = null;
    return function (fn) {
        clearTimeout(id);
        id = setTimeout(fn, ms);
    }
};

export const addLayersOffset = (layers, offset) => {
    let key, layer;
    for (key in layers) {
        layer = layers[key];
        layer.top += offset.top;
        layer.left += offset.left;
    }
    return layers;
};

export const removeLayersOffset = (layers, offset) => {
    let key, layer;
    for (key in layers) {
        layer = layers[key];
        layer.top -= offset.top;
        layer.left -= offset.left;
    }
    return layers;
};

export const getLayerActualSvg = (key, canvas) => {
    const obj = canvas.getObjects().find(obj => obj.__key === key);
    return obj ? obj.toSVG() : '';
};

export const downloadFile = (blob, name) => {
    const a = document.createElement('a');
    a.href = URL.createObjectURL(blob);
    a.download = name;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
};

export const getActiveInput = () => {
    const active = document.activeElement;
    if (active && (active.tagName === 'INPUT' || active.tagName === 'TEXTAREA')) {
        return active;
    } else {
        return null;
    }
};

export const getParameterByName = ((name) => {
    let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
});