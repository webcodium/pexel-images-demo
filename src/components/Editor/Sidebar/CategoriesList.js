import React, { Component } from 'react';
import classNames from 'classnames';
import _ from 'lodash';

import PexelsImages from "./PexelImages/PexelsImages";
import ScrollArea from 'react-scrollbar';
import { connect } from "react-redux";

class CategoriesList extends Component {

    getActiveBlockClass(blockName) {
        return classNames('category-item', blockName, { 'active': this.props.sidebar.current === blockName });
    };

    render() {
        const verticalContainerStyle = {
            background: '#1d1d1d',
            zIndex: 0
        };
        const verticalScrollbarStyle = {
            background: '#ccc',
            width: '6px',
            marginLeft: '4px'
        };

        return (
            <div className="category-frame">
                <div className={this.getActiveBlockClass('image')}>
                    <div className="frame-image-sidebar">
                        <h4 className="title">Images</h4>
                        <PexelsImages
                            delete={this.props.deleteItem}
                            addElement={this.props.addElement}
                            imagesType="image"
                            onDropReject={this.props.onDropReject}
                        />
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = store => {
    return {
        sidebar: store.sidebar,
        config: store.config
    }
};

const mapDispatchToProps = dispatch => ({});


export default connect(mapStateToProps, mapDispatchToProps)(CategoriesList);