import React, { Component } from 'react';
import classNames from 'classnames';
import ReactSVG from 'react-svg';
import IconButton from 'material-ui/IconButton';

import './Sidebar.css';
import CategoriesList from './CategoriesList';

import SidebarService from '../../../services/sidebarService';

import { setSidebarCurrent } from './../../../actions/sidebarAction';

import { connect } from "react-redux";


class Sidebar extends Component {

    constructor(...args) {
        super(...args);

        this.state = {
            showDeleteConfirm: false,
            deleteItem: null
        };
        this.sidebarItems = SidebarService.getItems(this.props.config);
        this.props.setSidebarCurrent({ open: true, current: this.getDefautSelected() });
    }

    getDefautSelected = () => {
        return 'image';
    }


    render() {
        const { config } = this.props;

        return (
            <aside className={classNames('main-sidebar', { 'empty': config.isSearchAd })}>
                {!config.isSearchAd &&
                    <div>
                        <div className="main-sidebar__nav">
                            {this.sidebarItems.map((item, index) => {
                                const attrs = {
                                    item: item,
                                    setSidebarCurrent: this.props.setSidebarCurrent,
                                    sidebar: this.props.sidebar,
                                    config: config,
                                    setSelection: this.props.setSelection
                                };
                                return <SideItem key={index} {...attrs} />;
                            })}
                        </div>


                        <section className='open'>
                            <div className="drop-sidebar__content">
                                <CategoriesList />
                            </div>
                        </section>
                    </div>}
            </aside>
        );
    }
}

const SideItem = (props) => {
    const buttonStyle = { textAlign: 'center', width: 36, fontSize: 0, height: 60 };
    const lowerName = props.item.name.toLowerCase();
    const setSidebarStatus = () => {
        props.setSidebarCurrent({ open: true, current: lowerName });
        props.setSelection(null);
    };

    return (<div>
        <div className={classNames('item', lowerName, { 'active': lowerName === props.sidebar.current })}>
            <IconButton style={buttonStyle} onTouchTap={setSidebarStatus}>
                <ReactSVG path={props.item.icon.src} className={props.item.icon.name} style={{ width: 30, height: 30 }} />
            </IconButton>
            <span className="side-tooltip">{props.item.name}</span>
        </div>
    </div>);
};


const mapStateToProps = store => {
    return {
        sidebar: store.sidebar,
        config: store.config
    }
};

const mapDispatchToProps = dispatch => ({
    setSidebarCurrent: (sidebar) => dispatch(setSidebarCurrent(sidebar)),
});


export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);