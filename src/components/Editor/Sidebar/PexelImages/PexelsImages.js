import React, { Component } from 'react';
import classNames from 'classnames';
import ReactSVG from 'react-svg';
import Tabs, { Tab } from 'material-ui/Tabs';
import Icons from './../../../../icons/icons';
import Image from './../../Elements/Image';
import Dropzone from 'react-dropzone';
import Masonry from 'react-masonry-component';
import axios from 'axios';
import { Loader } from 'react-loaders';

import ImageService from './../../../../services/imageService';
import ScrollArea from 'react-scrollbar';

// css
import './PexelImages.css';
import { addNewImages } from "./../../../../actions/sidebarAction";
import { setDropzoneLoader } from "./../../../../actions/dropzoneLoaderAction";

import { connect } from "react-redux";

class PexelsImages extends Component {

    constructor(...args) {
        super(...args);

        this.state = {
            activeTab: 0,
            pexelsImages: [],
            pexelsNextPage: '',
            searchText: '',
            loadNewPage: true,
        };
    }

    componentDidMount() {
        const searchTag = this.props.imagesType === 'background' ? 'texture' : 'art';
        this.getPexelsImages(searchTag, true);
    }

    formatImage(photos, type) {
        return photos.map(photo => {
            return {
                name: photo.photographer,
                path: photo.src.large,
                public: 1,
                type: type
            }
        });
    }

    loadNewPage(loadNewPage) {
        this.setState({
            loadNewPage
        })
    }

    getPexelsImages(search, new_search) {
        const query = search ? search : 'art';
        if (this.state.loadNewPage) {
            this.loadNewPage(false);

            if (new_search) {
                this.setState({ pexelsNextPage: '' });
            }

            ImageService.getPexelImages(query, new_search ? '' : this.state.pexelsNextPage).then(response => {
                const data = response.data;
                const newImages = new_search ? this.formatImage(data.photos, this.props.imagesType) : this.state.pexelsImages.concat(this.formatImage(data.photos, this.props.imagesType));
                this.setState({ pexelsImages: newImages, pexelsNextPage: data.next_page });
                this.loadNewPage(true);
            });
        }
    }

    setImageSearchLabel = event => {
        event.preventDefault();
        this.setState({ pexelsNextPage: '' });
        this.getPexelsImages(this.state.searchText, true);
    }

    handleChangeImageSearch = event => {
        this.setState({ searchText: event.target.value });
    }

    loadNextPage = value => {
        const { realHeight, containerHeight, topPosition } = value;
        if (!!topPosition && containerHeight + topPosition > realHeight - 200) {
            this.getPexelsImages(this.state.searchText);
        }
    }

    getImagesTabLabel(key) {
        switch (key) {
            case 'free':
                return (
                    <div>
                        <p>Free images</p>
                        <p>from <a href="https://www.pexels.com/" target="_blank">pexels</a></p>
                    </div>
                );
            case 'my':
                return (
                    <div>
                        <p>My</p>
                        <p>images</p>
                    </div>
                );
            default: {
                return null;
            }
        }
    }

    render() {
        const isAdmin = this.props.config.user_role === 'admin';
        const verticalContainerStyle = {
            background: '#1d1d1d',
            zIndex: 0
        };
        const verticalScrollbarStyle = {
            background: '#ccc',
            width: '6px',
            marginLeft: '4px'
        };
        return (
            <section className="frame-tabs-out">
                <div className="frame-tabs">
                    <Tabs value={this.state.activeTab}>
                        <Tab className="one-tab"
                            label={this.getImagesTabLabel('free')} />
                    </Tabs>
                </div>
                <div className="frame-content-tabs scroll-frame-out">
                    <ScrollArea
                        speed={0.8}
                        className="categories-wrapper"
                        contentClassName="content"
                        verticalContainerStyle={verticalContainerStyle}
                        verticalScrollbarStyle={verticalScrollbarStyle}
                        horizontal={false}
                        onScroll={this.loadNextPage}>
                        {this.state.activeTab === 0 &&
                            <PexelImg images={this.state.pexelsImages}
                                addElement={this.props.addElement}
                                type={this.props.imagesType}
                                setImageSearchLabel={this.setImageSearchLabel}
                                handleChangeImageSearch={this.handleChangeImageSearch} />
                        }
                    </ScrollArea>
                </div>
            </section>
        );
    }
}

const PexelImg = (props) => {
    const background = props.type === 'background';
    return (
        <section>
            {!background &&
                <section className="search-image-frame">
                    <form onSubmit={props.setImageSearchLabel}>
                        <input type="text" placeholder="Search for images" onChange={props.handleChangeImageSearch} />
                        <button type="submit">
                            <ReactSVG
                                path={Icons.iconSearch}
                                className="icon-search"
                                style={{ width: 21, height: 19 }}
                            />
                        </button>
                    </form>
                </section>
            }
            <Masonry
                className={'images-list'} // default ''
                elementType={'ul'} // default 'div'
                options={{ transitionDuration: 0 }} // default {}
                disableImagesLoaded={false} // default false
                updateOnEachImageLoad={false} // default false and works only if disableImagesLoaded is false
            >
                {props.images.map((image, index) => {
                    return (
                        <li className="item-image"
                            key={index}>
                            <Image data={image} type={image.type}
                                addElement={props.addElement} />
                        </li>
                    );
                })}
            </Masonry>
        </section>
    );
};

const mapStateToProps = store => {
    return {
        images: store.images,
        dropzoneLoader: store.dropzoneLoader,
        sidebar: store.sidebar,
        config: store.config
    }
};

const mapDispatchToProps = dispatch => ({
    addNewImages: (images) => dispatch(addNewImages(images)),
    setDropzoneLoader: (current) => dispatch(setDropzoneLoader(current)),
});


export default connect(mapStateToProps, mapDispatchToProps)(PexelsImages);