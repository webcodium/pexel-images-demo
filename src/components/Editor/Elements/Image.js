import React, { Component } from 'react';
import ReactSVG from 'react-svg';

import Icons from '../../../icons/icons';

class Image extends Component {

    state = { error: false };

    render() {
        return (
            <div className="element image">
                <img src={this.props.data.path} alt={this.props.data.name} onError={this.onLoadError} />
                {this.state.error &&
                    <div style={{ padding: 20 }} title="Image is not available">
                        <ReactSVG path={Icons.iconDropZonePic} style={{ width: 60, height: 30 }} />
                    </div>
                }
            </div>
        );
    }

    onLoadError = () => {
        this.setState({ error: true });
    }

    getImageConfig() {
        const { props } = this;
        return {
            type: props.type,
            imageUrl: props.data.path
        };
    }
}

export default Image;
