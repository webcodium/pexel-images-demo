import React, { Component } from 'react';

import './App.css';

import Sidebar from './components/Editor/Sidebar/Sidebar';

class App extends Component {
    render() {
        return (
            <div className="wrap">
                <div className='page-editor'>
                    <div style={{ backgroundColor: '#4d4d4d', height: '100vh' }}>
                        <Sidebar />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
