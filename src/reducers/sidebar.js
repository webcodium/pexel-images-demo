import {STORE_SIDEBAR, SET_SIDEBAR_CURRENT} from "./../actions/actionTypes";
import SidebarService from "../services/sidebarService";

const init = {
    open: true,
    current: 'templates'
};

function sidebar(state = init, action) {
    switch (action.type) {
        case STORE_SIDEBAR:
            return {...state, ...action.sidebar};
        case SET_SIDEBAR_CURRENT:
            return {...state, ...action.current};
        default:
            return state;
    }
}

export default sidebar;