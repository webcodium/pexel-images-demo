import {STORE_IMAGES, ADD_NEW_IMAGES, DELETE_IMAGE} from "./../actions/actionTypes";

const init = [];

function images(state = init, action) {
    switch (action.type) {
        case STORE_IMAGES:
            return [...state, ...action.images];
        case ADD_NEW_IMAGES:
            return [...action.images, ...state];
        case DELETE_IMAGE:
            return state.filter(item => item.id !== action.image.id);
        default:
            return state;
    }
}

export default images;