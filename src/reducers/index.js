import {combineReducers} from 'redux';

import sidebar from './sidebar';
import images from './images';
import dropzoneLoader from './dropzoneLoader';
import config from './config';

const reducers = combineReducers({
    sidebar,
    images,
    dropzoneLoader,
    config
});

export default reducers;