import {STORE_CONFIG, UPDATE_CONFIG_VAL} from "./../actions/actionTypes";

const init = {};

function config(state = init, action) {
    switch (action.type) {
        case STORE_CONFIG:
            return {...state, ...action.config};
        case UPDATE_CONFIG_VAL:
            return {...state, ...action.next};
        default:
            return state;
    }
}

export default config;