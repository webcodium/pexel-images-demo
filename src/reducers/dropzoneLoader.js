import {SET_DROPZONE_LOADER} from "./../actions/actionTypes";

const init = {
    loading: false,
    current: null
};

function dropzoneLoader(state = init, action = init) {
    switch (action.type) {
        case SET_DROPZONE_LOADER:
            return action.loader ? {...state, loading: true, current: action.loader} : {...state, ...init};
        default:
            return state;
    }
}

export default dropzoneLoader;