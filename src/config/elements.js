const defSet = {
    left: 0,
    top: 0
};

const elements = {
    image: {
        defaults: {
            ...defSet, ...{
                width: 110,
                height: 110,
                lockUniScaling: true
            }
        },
        fields: [
            '__key', '__type', '__zIndex', '__meta', '_element', 'animation',
            'left', 'top', 'url', 'width', 'height', 'angle',
            'scaleX', 'scaleY', 'path', 'opacity', 'selectable', 'hasControls',
            'lockMovementX', 'lockMovementY', 'hoverCursor', 'elSlideIndex'
        ],
        settings: {
            'Image': [{
                field: '_element',
                type: 'image',
                title: 'URL'
            }]
        }
    }
};

export default elements;
