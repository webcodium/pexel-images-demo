const _defConfig = {
    pexelCode: '563492ad6f9170000100000105e1db6fcbce474f7e43a53331791bae'
};

let _AppConfig;

const setConfig = data => {
    _AppConfig = data;
};

export default class ConfigService {

    static getConfig() {
        return _AppConfig;
    }

    static getConfigProp(prop) {
        return _AppConfig[prop];
    }

    static loadConfig() {
        return new Promise((resolve, reject) => {
            const _newConfig = {
                ..._defConfig,
                width: 500,
                height: 500
            };
            setConfig(_newConfig);
            resolve(_newConfig);
        });
    }

}