import Icons from '../icons/icons';

const items = [
    {
        name: "Image",
        type: 'image',
        icon: {
            name: 'icon-tab-images',
            src: Icons.iconTabImages
        }
    }
];

export default class SidebarService {
    static getItems(config) {
        return items;
    }
}