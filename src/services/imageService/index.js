import axios from 'axios';
import bowser from 'bowser';
import uuid from 'uuid';

import ConfigService from '../configService';
import imageNotAvailableSrc from './imageNotAvailable';

export default class ImageService {

    static getPexelImages(query, new_page) {
        let link = new_page ? new_page : `https://api.pexels.com/v1/search?page=1&per_page=25&query=${query}`;
        return axios.get(link, { headers: { 'Authorization': ConfigService.getConfigProp('pexelCode') } });
    }

    static loadImage(url) {
        if (bowser.chrome) {
            url = new URL(url);
            url.searchParams.set('crs', uuid());
            url = url.toString();
        }

        return new Promise(resolve => {
            const img = new Image();
            img.crossOrigin = 'anonymous';
            img.src = url;
            img.onload = () => resolve(img);
            img.onerror = () => {
                const palceholderImg = new Image();
                palceholderImg.src = imageNotAvailableSrc;
                resolve(palceholderImg);
            }
        });
    }

}